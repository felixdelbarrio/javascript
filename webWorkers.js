if (Worker == "undefined") {
    document.getElementById("MensajeWorker").innerHTML = "Web Workers no disponibles";
}

var worker = new Worker("tarea.js");

//Esto establece el parámetro que se pasa al worker
worker.postMessage("Datos");

// Cuando el proceso termina se pasa por este método
worker.onmessage = function (event) {
    var message = "Mensaje del Worker: " + event.data;
    // Actualiza la interfaz de usuario
    document.getElementById("MensajeWorker").innerHTML = message;
}

// Asignamos la función a ejecutar
onmessage = mensajeVuelta;

function mensajeVuelta(event) {
    // El objeto event transporta la información
    // en su propiedad data
    if (event.data == "Datos") {
        postMessage("Datos de vuelta");
    }
    else {
        // Error intencionado
        1/x;
    }
}