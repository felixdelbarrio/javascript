function storyUserData() {
    console.log('storyUserData');
    
    //useful when: Clicking on a "Submit" button, prevent it from submitting a form
    event.preventDefault();

    var name = document.getElementById("formNombre").value;
    console.log("El nombre es: " + name);
    var email = document.getElementById("formEmail").value;
    console.log("El email es: " + email);

    var userData = {
        "name":name ,
        "email":email
    };

    localStorage.setItem("userDataJSON" , JSON.stringify(userData));

}

function getUserData(){
    console.log('getUserData');

     //useful when: Clicking on a "Submit" button, prevent it from submitting a form
     event.preventDefault();

    var userData = JSON.parse(localStorage.getItem("userDataJSON"));
    console.log(userData);
    
    document.getElementById("formNombre").value = userData.name;
    document.getElementById("formEmail").value = userData.email;

}