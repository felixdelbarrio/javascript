function storyUserData() {
    console.log('storyUserData');
    
    //useful when: Clicking on a "Submit" button, prevent it from submitting a form
    event.preventDefault();

    var name = document.getElementById("formNombre").value;
    console.log(name);

    sessionStorage.setItem("name", name);
    localStorage.setItem("name", name);
}

function getUserData(){
    console.log('getUserData');

     //useful when: Clicking on a "Submit" button, prevent it from submitting a form
     event.preventDefault();

    var nameInput = document.getElementById("formNombre");
    nameInput.value = localStorage.getItem("name");

}