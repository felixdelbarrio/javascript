function leerVentas() {
    var url = "http://localhost:1565/DemosJS/ventas.json";
    var request = new XMLHttpRequest();
    request.responseType = "application/json"; //Predeterminado
    request.open("GET", url); //Solo configura la llamada
    request.onload = function() {
        if (request.status == 200) { //200 significa correcto.
            actualizarIU(request.responseText); }
        };
    request.send(null);
}

function actualizarIU(respuestaJSON) {
    var DivVentas = document.getElementById("DIV_ventas");
    var ventas = JSON.parse(respuestaJSON);
    for (var i = 0; i < ventas.length; i++) {
        var venta = ventas[i];
        var div = document.createElement("div");
        div.setAttribute("class", "FormatoVenta");
        div.innerHTML = "Total ventas de " + venta.name + ": " + venta.TVentas;
        DivVentas.appendChild(div);
    }    
}
//En el caso de que tengamos que dar soporte a algunos navegadores muy antiguos deberíamos comprobar el evento onreadystatechange
function leerVentas_XHRv1() {
    var url = "http://localhost:1565/DemosJS/ventas.json";
    var request = new XMLHttpRequest();
    request.open("GET", url);
    request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status == 200) {
        actualizarIU(request.responseText); }
    };
    request.send(null);
}